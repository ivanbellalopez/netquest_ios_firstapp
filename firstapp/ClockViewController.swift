//
//  ClockViewController.swift
//  firstapp
//
//  Created by Ivan Bella López on 11/07/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

import UIKit

class ClockViewController: UIViewController {
    
    @IBOutlet private weak var welcomeLabel: UILabel?
    @IBOutlet private weak var timeLabel: UILabel?
    var nameString: String!
    var dateString: String! {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss.SSS"
        return dateFormatter.stringFromDate(NSDate())
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = nameString
        welcomeLabel?.text = "Hello " + nameString + "!"
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(1/100, target: self, selector: Selector("showTimeAnimated"), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Private
    func showTimeAnimated() {
        timeLabel?.text = dateString
    }
}
