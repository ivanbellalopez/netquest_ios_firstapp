//
//  InputViewController.swift
//  firstapp
//
//  Created by Ivan Bella López on 10/07/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

import UIKit

extension UITextField {
    func isValidText () -> Bool {
        return count(self.text.utf8) > 0
            && self.text.stringByTrimmingCharactersInSet(NSCharacterSet.letterCharacterSet()) == ""
    }
}


class InputViewController: UIViewController {
    
    @IBOutlet private weak var textField: UITextField?
    @IBOutlet private weak var submitButton: UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Welcome"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Touches
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        textField?.resignFirstResponder()
    }
    
    
    // MARK: - Actions
    @IBAction func submitAction(sender: AnyObject) {
        textField?.resignFirstResponder()
        attemptShowNextScreen()
    }
    
    
    // MARK: - Private
    func attemptShowNextScreen() {
        if textField?.isValidText() == true {
            performSegueWithIdentifier("nextSegue", sender: nil)
        } else {
            let alertController = UIAlertController(title: "Error", message: "A valid name is mandatory", preferredStyle: UIAlertControllerStyle.Alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:nil)
            
            alertController .addAction(alertAction)
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let clockViewController = segue.destinationViewController as! ClockViewController
        clockViewController.nameString = textField?.text
    }
}
